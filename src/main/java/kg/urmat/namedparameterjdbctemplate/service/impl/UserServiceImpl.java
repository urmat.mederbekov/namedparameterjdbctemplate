package kg.urmat.namedparameterjdbctemplate.service.impl;

import kg.urmat.namedparameterjdbctemplate.dao.UserDAO;
import kg.urmat.namedparameterjdbctemplate.model.User;
import kg.urmat.namedparameterjdbctemplate.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserDAO userDAO;

    @Override
    public List<User> getAll() {
        return userDAO.getAll();
    }

    @Override
    public List<User> getAllByCompanyId(Long companyId) {
        return userDAO.getAllByCompanyId(companyId);
    }
}
