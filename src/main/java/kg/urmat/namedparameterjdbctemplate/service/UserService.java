package kg.urmat.namedparameterjdbctemplate.service;

import kg.urmat.namedparameterjdbctemplate.model.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    List<User> getAllByCompanyId(Long companyId);
}
