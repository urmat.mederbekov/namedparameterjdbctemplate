package kg.urmat.namedparameterjdbctemplate.model;

import kg.urmat.namedparameterjdbctemplate.model.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class User extends BaseEntity {

    private String firstName;

    private String lastName;
    
    private List<CompanyDepartment> companyDepartments;
}
