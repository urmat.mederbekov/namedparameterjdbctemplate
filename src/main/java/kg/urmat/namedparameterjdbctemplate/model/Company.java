package kg.urmat.namedparameterjdbctemplate.model;

import kg.urmat.namedparameterjdbctemplate.model.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Company extends BaseEntity {

    private String name;

}
