package kg.urmat.namedparameterjdbctemplate.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;

@Getter
@Setter
public class CompanyDepartment {

    @JsonProperty(value = "company_department_id")
    private Long id;

    @JsonProperty(value = "company_id")
    private Long companyId;

    @JsonProperty(value = "department_parent")
    private Long departmentParentId;

    @JsonProperty(value = "department_name")
    private String name;

    @JsonProperty(value = "department_description")
    private String description;

    @JsonProperty(value = "department_date_added")
    private OffsetDateTime dateAdded;

    @JsonProperty(value = "department_active")
    private int active = 0;

    @JsonProperty(value = "invite_token")
    private String inviteToken;

    @JsonProperty(value = "order_number")
    private int orderNumber;
}
