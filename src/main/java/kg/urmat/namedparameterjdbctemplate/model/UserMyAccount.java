package kg.urmat.namedparameterjdbctemplate.model;

import kg.urmat.namedparameterjdbctemplate.model.base.BaseEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserMyAccount extends BaseEntity {

    private Long userId;

    private Long companyId;
}
