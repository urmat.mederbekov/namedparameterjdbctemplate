package kg.urmat.namedparameterjdbctemplate.dao;

import kg.urmat.namedparameterjdbctemplate.model.User;

import java.util.List;

public interface UserDAO {

    List<User> getAll();

    List<User> getAllByCompanyId(Long companyId);
}
