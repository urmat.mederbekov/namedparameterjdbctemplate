package kg.urmat.namedparameterjdbctemplate.dao.impl;

import kg.urmat.namedparameterjdbctemplate.dao.UserDAO;
import kg.urmat.namedparameterjdbctemplate.model.User;
import kg.urmat.namedparameterjdbctemplate.rowMapper.UserRowMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class UserDAOImpl implements UserDAO {

    private final NamedParameterJdbcTemplate template;

    public List<User> getAll() {

        final String select =
                "SELECT u.*,(\n" +
                "    SELECT json_agg(cd)\n" +
                "    FROM company_department cd\n" +
                "        INNER JOIN company_department_manager cdm on cd.company_department_id = cdm.company_department_id\n" +
                "    WHERE cdm.user_id = u.user_id\n" +
                "    ) as departments\n" +
                "FROM \"user\" u\n";

        return template.query(select, new UserRowMapper());
    }

    public List<User> getAllByCompanyId(Long companyId) {

        final SqlParameterSource parameters = new MapSqlParameterSource().addValue("company_id", companyId);
        final String select =
                "SELECT u.*,(\n" +
                "    SELECT json_agg(cd)\n" +
                "    FROM company_department cd\n" +
                "        INNER JOIN company_department_manager cdm on cd.company_department_id = cdm.company_department_id\n" +
                "    WHERE cdm.user_id = u.user_id\n" +
                "    ) as departments\n" +
                "FROM \"user\" u\n" +
                "         INNER JOIN user_myaccount um on u.user_id = um.user_id\n" +
                "WHERE company_id = :company_id";

        return template.query(select, parameters, new UserRowMapper());
    }
}
