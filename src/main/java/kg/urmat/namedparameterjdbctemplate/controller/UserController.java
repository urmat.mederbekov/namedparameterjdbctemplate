package kg.urmat.namedparameterjdbctemplate.controller;

import kg.urmat.namedparameterjdbctemplate.model.User;
import kg.urmat.namedparameterjdbctemplate.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public List<User> getAll(){
        return userService.getAll();
    }

    @GetMapping("/by-company-id/{companyId}")
    public List<User> getAll(@PathVariable Long companyId){
        return userService.getAllByCompanyId(companyId);
    }
}
