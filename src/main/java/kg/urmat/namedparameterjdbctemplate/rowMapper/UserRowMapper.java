package kg.urmat.namedparameterjdbctemplate.rowMapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import kg.urmat.namedparameterjdbctemplate.model.CompanyDepartment;
import kg.urmat.namedparameterjdbctemplate.model.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static java.util.Objects.nonNull;

public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        final User user = new User();
        user.setId(rs.getLong("user_id"));
        user.setFirstName(rs.getString("user_first_name"));
        user.setLastName(rs.getString("user_last_name"));

        ObjectMapper objectMapper = new ObjectMapper().findAndRegisterModules();
        if (nonNull(rs.getString("departments"))) {
            try {
                List<CompanyDepartment> departments = objectMapper.readValue(rs.getString("departments"), new TypeReference<>() {});
                user.setCompanyDepartments(departments);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return user;
    }
}
