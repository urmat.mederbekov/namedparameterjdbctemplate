create table company
(
    company_id   bigserial
        primary key,
    company_name varchar(500)
);


create table company_department
(
    company_department_id  bigserial
        primary key,
    company_id             bigint                             not null,
    department_parent      bigint,
    department_name        varchar(200),
    department_description varchar(2500),
    department_date_added  timestamp with time zone default CURRENT_TIMESTAMP,
    department_active      integer                  default 1 not null,
    invite_token           varchar(255),
    order_number           integer                  default 0
);

create table company_department_manager
(
    company_department_manager_id bigserial primary key,
    company_department_id         bigint not null,
    user_id                       bigint not null
);

create table user_myaccount
(
    user_myaccount_id bigserial
        constraint user_myaccount_pkey
            primary key,
    user_id           bigint,
    company_id        bigint
);

create table "user"
(
    user_id         bigserial
        constraint user_pkey
            primary key,
    user_last_name  varchar(200),
    user_first_name varchar(200)
);
